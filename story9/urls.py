from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.pertama, name = 'pertama'),
    path('login', views.login_ya, name = 'login'),
    path('signup', views.signup_ya, name = 'signup'),
    path('logout', views.logout_ya, name = 'logout'),
    
]
