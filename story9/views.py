from django.shortcuts import render,redirect 
from .forms import SignupForm, AccountForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages


# Create your views here.
def pertama(request):
    nama = ''
    if 'nama' in request.session:
        print('pertama')
        nama = request.session['nama']
    else :
        print('tidak')
    context = {'name' : nama,}
    return render(request, 'pertama.html', context)


def login_ya(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                request.session['nama'] = username
                return redirect('/story9')
        else:
            messages.info(request, 'Your username or password is incorrect.')
    context = {}
    return render(request, 'login.html', context)


def signup_ya(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        account_form = AccountForm(request.POST)
        if form.is_valid():
            user = form.save()
            account = account_form.save(commit=False)
            account.user = user
            account.name = user.username
            account.email = user.email
            account.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            try:    
                request.session['nama'] = username
                print("berhasil")
            except:
                print('halo')
            login(request, user)
            print(request.session.keys())
            return redirect('/story9')
    else:
        form = SignupForm()
        account_form = AccountForm()

    context = {'form': form, 'account_form': account_form}
    return render(request, 'signup.html', context)

@login_required
def logout_ya(request):
    if request.session.has_key ('nama'):
        request.session.flush()
        logout(request)
    else:
        logout(request)
    return redirect('/story9')