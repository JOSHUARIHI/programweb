from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse

from . import views, models
from .views import *
from .models import Akun, User

# Create your tests here.
class Test_story9(TestCase):
    
	def test_home_page(self):
		response = Client().get('/story9/')
		self.assertEqual(response.status_code, 200)	
	def test_template_home (self):
		response = Client().get('/story9/')
		self.assertTemplateUsed(response,'pertama.html')
	def tes_views_home(self):
		hasil = resolve('/story9/')
		self.assertEqual(hasil.func, views.pertama)
  
	def test_login_page(self):
		response = Client().get('/story9/login')
		self.assertEqual(response.status_code, 200)
	def test_template_login(self):
		response = Client().get('/story9/login')
		self.assertTemplateUsed(response, 'login.html')
	def test_view_login(self):
		response = resolve('/story9/login')
		self.assertEqual(response.func, views.login_ya)
  
	def test_signup_page(self):
		response = Client().get('/story9/signup')
		self.assertEqual(response.status_code, 200)
	def test_template_signup(self):
		response = Client().get('/story9/signup')
		self.assertTemplateUsed(response,'signup.html')
	def test_view_signup(self):
		response = resolve('/story9/signup')
		self.assertEqual(response.func,	views.signup_ya)
	
	def test_logout_page(self):
		response = Client().get('/story9/logout')
		self.assertEqual(response.status_code, 302)
	def test_view_logout(self):
		response = resolve('/story9/logout')
		self.assertEqual(response.func, views.logout_ya)
  
	def test_login (self):
		self.credentials = {
			'username' : 'tests',
			'password' : 'rahasia',
		}
		User.objects.create_user(**self.credentials)
		response = self.client.post('/story9/login', self.credentials, follow=True)
		self.assertTrue(response.context['user'].is_active)
	def test_logout(self):
		self.credentials = {
			'username' : 'tests',
			'password' : 'rahasia',
		}
		User.objects.create_user(**self.credentials)
		self.client.login(username = 'tests', password= 'rahasia')
		response = self.client.get(reverse('story9:logout'),{})
		self.assertEqual(response.status_code, 302)
		
	def test_login_salah (self):
		self.credentials = {
			'username' : 'tests',
			'password' : 'rahasia',
		}
		User.objects.create_user(**self.credentials)
		yang_salah = {
			'username' : 'tests',
			'password' : '1231josh',
		}
		response = self.client.post('/story9/login', yang_salah, follow=True)
		self.assertFalse(response.context['user'].is_active)
	def test_signup(self):
		data_kirim = {
			'username'	: 'joshua',
			'email'		: 'Joshua@gmail.com',
			'password1'	: 'vivaruseis',
			'password2'	: 'vivaruseis',
			'age'		: 18,
		}
		response = self.client.post(reverse('story9:signup'), data_kirim)
		self.assertEqual (response.status_code, 302)
		self.assertEqual(Akun.objects.all().count(), 1)