from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.core.validators import MinValueValidator

# Create your models here.

class Akun(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	name = models.CharField(max_length=150, null=True)
	email = models.EmailField(max_length=200, null=True)
	age  = models.PositiveIntegerField(validators=[MinValueValidator(1)])

	def __str__(self):
		return self.name
	

	