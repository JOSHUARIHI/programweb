from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Akun

class SignupForm(UserCreationForm):
	class Meta(UserCreationForm.Meta):
		fields = UserCreationForm.Meta.fields + ("email",)
		labels = {
			'email': 'Email',
		}

class AccountForm(forms.ModelForm):
	class Meta:
		model = Akun
		fields = ['age']
