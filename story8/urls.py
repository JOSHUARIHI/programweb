from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.mulai, name = 'mulai'),
    path('/baru/', views.baru, name = 'baru'),
]
