from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.
def mulai(request):
    return render(request, 'story8.html')

def olah(request):
    args = request.GET['q']
    tujuan = "https://www.googleapis.com/books/v1/volumes?q=" + args
    hasil =  requests.get(tujuan)
    var = json.loads(hasil.content)

    return JsonResponse (var, safe=False)

def baru(request):
    return render(request, 'baru.html')
