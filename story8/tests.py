from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
# Create your tests here.
class story8(TestCase):
    def test_home_page(self):
        response = Client().get('/story8')
        self.assertEqual(response.status_code, 200)
        
    def test_template_home(self):
        response = Client().get('/story8')
        self.assertTemplateUsed(response, 'story8.html')
        
    def test_func_home(self):
        found = resolve('/story8')
        self.assertEqual(found.func, views.mulai)
        
    def test_template_olah(self):
        found = resolve('/cari/')
        self.assertEqual(found.func, views.olah)
        
    def test_func_olah(self):
        response = Client().get("/cari/?q=halo%202")
        self.assertEqual(response.status_code, 200)