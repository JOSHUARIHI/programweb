$(document).ready(function() {
    $("#search").keyup(function() {
        var isi_ketik = $("#search").val();
        //console.log(isi_ketik);
        var url_terpanggill = "/cari?q=" + isi_ketik;
        //console.log(url_terpanggill);
        $.ajax({
            url: url_terpanggill,
            success: function(hasil) {
                console.log(hasil.items);

                var yang_ditaruh = $(".hasil");
                yang_ditaruh.empty();

                var yang_kedua = $(".sambut");
                yang_kedua.empty();

                var yang_ketiga = $(".tabel");
                yang_ketiga.empty();
                yang_ketiga.append(`
                    <tr>
                        <th>Sampul</th>
                        <th>Judul</th> 
                        <th>Penulis</th>
                    </tr>
                `);
                for (i = 0; i < hasil.items.length; i++) {
                    var title = hasil.items[i].volumeInfo.title;
                    var penulis = hasil.items[i].volumeInfo.authors[0];
                    var foto = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                    var detail = hasil.items[i].id;
                    console.log(hasil.items[i]);
                    console.log(detail);
                    yang_ditaruh.append(`
                    <div class="row">
                        <div class="col s12 m7">
                            <div class="card">
                                <div class="card-image">
                                    <img src="` + foto + `">
                                </div>
                                <div class="card-content">
                                    <span class="card-title">` + title + `</span>
                                    <p> Ditulis oleh ` + penulis + `</p>
                                </div>
                                <div class="card-action">
                                    <a class="waves-effect waves-light btn" href = "https://play.google.com/store/books/details?id=` + detail + `&source=gbs_api">More Info</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    `);
                    yang_ketiga.append(`
                        <tr>
                            <td><img src="` + foto + `"></td>
                            <td>` + title + `</td>
                            <td>` + penulis + `</td>
                        </tr>
                    `);
                }
            }
        });
    });
})