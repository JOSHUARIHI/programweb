from django import  forms
from .models import kuliah

class inputjadwal(forms.Form):
    Class = forms.CharField(required = True)
    Dosen = forms.CharField(required = True)
    Sks = forms.DecimalField(required = True)
    semester = forms.ChoiceField(choices = [('pertama', 'Gasal 2019/2020'), ('genap','Genap 2019/2020'), ('ketiga', 'Gasal 2020/2021')])
    deskripsi = forms.CharField(widget=forms.Textarea, required = False)
    
class kuliah_form (forms.ModelForm):
    class Meta:
        model = kuliah
        fields = "__all__"
        
        
   