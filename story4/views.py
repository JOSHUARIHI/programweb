from django.shortcuts import render, redirect
from .forms import inputjadwal, kuliah_form
from .models import kuliah
import random

# Create your views here.
def home(request):
    return render(request, 'home.html')
def profile(request):
    return render(request, 'profile.html')
def portofolio(request):
    return render(request, 'portofolio.html')
def education(request):
    return render(request, 'education.html')
def skills(request):
    return render(request, 'skills.html')
def contact(request):
    return render(request, 'contact.html')

def form(request):
    if request.method == 'POST':
        form = inputjadwal(request.POST)
        if form.is_valid():
            clas = form.cleaned_data['Class']
            dosen = form.cleaned_data['Dosen']
            print (clas,dosen)
        
    form = inputjadwal()
    return render (request, 'Form.html', {'form' : form})



def delete(request, nama):
    kuliah.objects.filter(Class=nama).delete()
    return redirect('/result')

def jadwal(request):
    if request.method == 'POST':
        form = kuliah_form(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('/form')
    form = kuliah_form()
    return render (request, 'Form.html', {'form' : form})

def hasil(request):
    post = kuliah.objects.all()
    context = {
        'jadwal' : post
    }
    return render(request, 'result.html', context)

def hasil2(request,nama):
    post = kuliah.objects.get(Class = nama)
    context = {
            'nama' : post
    }
    return render(request, 'hasil2.html', context)