from django.db import models

# Create your models here.
class kuliah(models.Model):
    Class = models.CharField(max_length = 20)
    Dosen = models.CharField(max_length = 20)
    sks_choices = (('1', '1'), ('2','2'), 
                    ('3', '3'), ('4', '4'),
                    ('5', '5'), ('6', '6'))
    Sks = models.CharField(max_length = 20, choices = sks_choices, default='1')
    semester_choices = (('Gasal 2019/2020', 'Gasal  2019/2020-1'), 
                        ('Genap 2019/2020', 'Genap 2019/2020-2'), 
                        ('Gasal 2020/2021', 'Gasal  2020/2021-3'),
                        ('Genap 2020/2021', 'Genap 2020/2021-4'),
                        ('Gasal 2021/2022', 'Gasal  2021/2022-5'),
                        ('Genap 2021/2022', 'Genap 2021/2022-6'),
                        ('Gasal 2022/2023', 'Gasal  2022/2023-7'),
                        ('Genap 2022/2023', 'Genap 2022/2023-8'),
                        )
    semester = models.CharField(max_length = 20, choices = semester_choices,default='Gasal  2019/2020-1')
    ruangan = models.CharField(max_length = 1,default="")
    deskripsi = models.TextField()
    
    def __str__(self):
        return self.Class