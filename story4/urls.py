from django.urls import path

from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.home, name = 'home'),
    path('home', views.home, name = 'home'),
    path('profile', views.profile, name = 'profile'),
    path('education', views.education, name = "education"),
    path('portofolio', views.portofolio, name = "portofolio"),
    path('skills', views.skills, name = "skills"),
    path('contact', views.contact, name = "contact"),
    path('form', views.jadwal, name ="form"),
    path('result', views.hasil, name ="result"),
    path('delete/<nama>', views.delete, name= "delete"),
    path('detail/<nama>', views.hasil2, name= "hasil2")
    
]
