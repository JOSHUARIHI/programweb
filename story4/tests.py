from django.test import TestCase, Client

# Create your tests here.
from django.urls import resolve
from .models import kuliah
from . import views

class story4(TestCase):
    
    def test_home_page (self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        
    def test_home_template (self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'home.html')
        
    def test_home_func (self):
        response = resolve('/')
        self.assertEqual(response.func, views.home)
   