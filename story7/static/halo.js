$(document).ready(function() {
    $(".card-header").click(function() {
            $(this).next(".card-isi").slideToggle()
        })
        /*$('#item1').click(function() {
            $("#isi1").slideToggle(300);
        });*/
    $(".reorder-up").click(function() {
        var $current = $(this).closest('.card')
        var $previous = $current.prev('.card');
        if ($previous.length !== 0) {
            $current.insertBefore($previous);
        }
        return false;
    });

    $(".reorder-down").click(function() {
        var $current = $(this).closest('.card')
        var $next = $current.next('.card');
        if ($next.length !== 0) {
            $current.insertAfter($next);
        }
        return false;
    });
})