from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views

# Create your tests here.

class Story6(TestCase):
    def test_home_page(self):
        response = Client().get('/story7')
        self.assertEqual(response.status_code, 200)
        
    def test_template_home(self):
        response = Client().get('/story7')
        self.assertTemplateUsed(response, 'story7.html')
        
    def test_func_home(self):
        found = resolve('/story7')
        self.assertEqual(found.func, views.awal)