from django.test import TestCase
from django.test import Client
from django.urls import resolve

from .models import kegiatan, anggota
from . import views


# Create your tests here.
class Story6(TestCase):
    def test_home_page(self):
        response = Client().get('/story6')
        self.assertEqual(response.status_code, 200)
    def test_template_home(self):
        response = Client().get('/story6')
        self.assertTemplateUsed(response, 'daftar_kegiatan.html')
    def test_func_home(self):
        found = resolve('/story6')
        self.assertEqual(found.func, views.rumah)
    def test_story6_model_kegiatan(self):
        kegiatan.objects.create(nama_kegiatan='HALO')
        Kegiatan = kegiatan.objects.get(nama_kegiatan='HALO')
        self.assertEqual(str(Kegiatan), 'HALO')

    def test_home_page(self):
        response = Client().get('/story6')
        self.assertEqual(response.status_code, 200)
        
    def test_template_home(self):
        response = Client().get('/story6')
        self.assertTemplateUsed(response, 'daftar_kegiatan.html')
        
    def test_func_home(self):
        found = resolve('/story6')
        self.assertEqual(found.func, views.rumah)
    
    def test_daftar_kegiatan(self):
        hai = kegiatan.objects.create(nama_kegiatan = "MASAK")
        dicari = Client().get('/story6/tambah_anggota/<namakegiatan>', args=[hai])
        kawan = kegiatan.objects.get(nama_kegiatan = 'MASAK')
        self.assertEqual(str(kawan.nama_kegiatan), "MASAK")
        
    def test_daftar_page(self):
        hai = kegiatan.objects.create(nama_kegiatan = "MASAK")
        dicari = Client().get('/story6/tambah_anggota/<namakegiatan>', args=[hai])
        self.assertEqual(dicari.status_code, 200)
            
    def test_story6_model_kegiatan(self):
        kegiatan.objects.create(
            nama_kegiatan='AJIK',
        )
        Kegiatan = kegiatan.objects.get(nama_kegiatan='AJIK')
        self.assertEqual(str(Kegiatan), 'AJIK')
    
    def test_story6_anggota(self):
        hai = kegiatan.objects.create(nama_kegiatan = "MASAK")
        anggota.objects.create(
            nama_anggota = 'yuyan',
            nama_kegiatan = hai
        )
        kawan = anggota.objects.get(nama_anggota = 'yuyan')
        self.assertEqual(str(kawan.nama_kegiatan), "MASAK")
        
    def test_story6_detail(self):
        hai = kegiatan.objects.create(nama_kegiatan = "MASAK")
        dicari = Client().get('/story6/detail_anggota/<namakegiatan>', args=[hai])
        self.assertEqual(dicari.status_code, 200)
        
    def test_story6_detail_args(self):
        dicari = Client().get('/story6/detail_anggota/<namakegiatan>', args=[kegiatan.objects.create(nama_kegiatan = "MASAK")])
        self.assertTemplateUsed(dicari, 'detail_anggota.html')

