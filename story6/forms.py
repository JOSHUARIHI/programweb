from django import forms
from .models import kegiatan, anggota

class form_kegiatan(forms.ModelForm):
    class Meta:
        model = kegiatan
        fields = "__all__"
        
class form_anggota(forms.ModelForm):
    class Meta:
        model = anggota
        fields = (('nama_anggota'),)