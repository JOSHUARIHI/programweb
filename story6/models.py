from django.db import models

# Create your models here.


class kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length = 20);
    
    
    def __str__(self):
        return self.nama_kegiatan
    
    
class anggota (models.Model):
    nama_anggota = models.CharField(max_length = 20);
    nama_kegiatan = models.ForeignKey(kegiatan,default = None, on_delete = models.DO_NOTHING)
    
    def __str__(self):
        return self.nama_anggota
    