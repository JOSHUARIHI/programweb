from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.rumah, name = 'rumah'),
    path('/tambah_anggota/<namakegiatan>', views.tambah, name= "tambah"),
    path('/detail_anggota/<namakegiatan>', views.detail, name= "detail")
]
