from django.shortcuts import render, redirect
from .forms import form_kegiatan, form_anggota
from .models import anggota, kegiatan


# Create your views here.
def rumah(request):
    if request.method == 'POST':
        form = form_kegiatan (request.POST)
        if form.is_valid():
            form.save()
    form = form_kegiatan()
    Anggota = anggota.objects.all()
    Kegiatan = kegiatan.objects.all()
    
    context = {
        'form' : form,
        'KEGIATAN' : Kegiatan,
        'ANGGOTA' : Anggota
    }
    return render(request, 'daftar_kegiatan.html', context)

def tambah(request, namakegiatan):
    if request.method == 'POST':
        form = form_anggota (request.POST)
        if form.is_valid():
            Kegiatan = kegiatan.objects.get(nama_kegiatan = namakegiatan)
            instance = form.save(commit = False)
            instance.nama_kegiatan = Kegiatan
            instance.save()
            return redirect('/story6')
    form = form_anggota()
    context = {
        'form' : form,
    }
    return render(request, 'daftar_anggota.html',context)

def detail(request, namakegiatan):
    Anggota = anggota.objects.all()
    context = {
        'ANGGOTA' : Anggota,
        'KEGIATAN' : namakegiatan
    }
    return render(request, 'detail_anggota.html', context)
    